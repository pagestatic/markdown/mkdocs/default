https://www.mkdocs.org/s

Если у вас есть и вы используете менеджер пакетов (например, apt-get , dnf , homebrew , yum , chocolatey и т. д.) для установки пакетов в вашей системе, тогда вы можете хотите найти пакет "MkDocs" и, если доступна последняя версия, установите его с помощью диспетчера пакетов (проверьте документацию по вашей системе, подробности). Вот и все, готово! Переходите к началу работы . 

<!-- Alex@Air Default % brew install MkDocs
==> Downloading https://ghcr.io/v2/homebrew/core/mkdocs/blobs/sha256:c632678896672c74e404e891f8ed7c94fbbef5d73c38397f0f77398ea98d8162
==> Downloading from https://pkg-containers.githubusercontent.com/ghcr1/blobs/sha256:c632678896672c74e404e891f8ed7c94fbbef5d73c38397f0f77398ea98d8162?se=
######################################################################## 100.0%
==> Pouring mkdocs--1.1.2_1.big_sur.bottle.tar.gz
🍺  /usr/local/Cellar/mkdocs/1.1.2_1: 1,673 files, 26.3MB
Alex@Air Default %  -->

mkdocs new MkDocs
cd /Volumes/Jam/Sites/Markdown/MkDocs

Есть единственный файл конфигурации с именем mkdocs.ymlи папка с именем docsкоторый будет содержать ваши исходные файлы документации. Прямо сейчас docs папка содержит только одну страницу документации с именем index.md.

MkDocs поставляется со встроенным dev-сервером, который позволяет просматривать вашу документацию. как вы над этим работаете. Убедитесь, что вы находитесь в том же каталоге, что и mkdocs.yml файл конфигурации, а затем запустите сервер, запустив mkdocs serve команда:

$ mkdocs serve
INFO    -  Building documentation...
INFO    -  Cleaning site directory
[I 160402 15:50:43 server:271] Serving on http://127.0.0.1:8000
[I 160402 15:50:43 handlers:58] Start watching changes
[I 160402 15:50:43 handlers:60] Start detecting changes

Открыть http://127.0.0.1:8000/в вашем браузере, и вы увидите значение по умолчанию отображается домашняя страница: 

